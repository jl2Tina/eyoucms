<!doctype html>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
<html>
<head>
<meta charset="UTF-8" />
<meta http-equiv="Content-Language" content="zh-cn"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<title><?php echo $Title; ?> - <?php echo $Powered; ?></title>
<link rel="stylesheet" href="./css/install.css?v=v1.3.1" />
<script src="./js/jquery.js?v=v1.3.1"></script> 
</head>
<body>
<div class="wrap">
  <?php require './templates/header.php';?>
  <div class="section">
  	<div class="blank30"></div>
  	<div class="go"></div>
  	<div class="blank30"></div>
    <div class="main cc">
    	<div class="wraper">
      <pre class="pact" readonly="readonly">
      	<div class="blank20"></div>
<p style="font-size:13px;font-weight:bold;margin-bottom:10px;">嗨，您好！欢迎使用	苏州合生源集成房屋科技有限公司</p>

<p>版权所有 (c)2021，苏州合生源集成房屋科技有限公司。 </p>
<div class="blank20"></div>
</pre>
</div>
    
    </div>
    <div class="blank20"></div>
    <div class="bottom tac"> <center><a id="next_submit" href="<?php echo $_SERVER['PHP_SELF']; ?>?step=2" class="btn_a">接 受</a></center> </div>
  </div>
</div>
<div class="blank30"></div>
<?php require './templates/footer.php';?>

<script type="text/javascript">
  $(function(){
    $('#next_submit').focus();
  });
</script>
</body>
</html>